<?php
/**
 * https://gitee.com/Mao02
 * http://www.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2019/4/17
 * Time: 14:30
 */

class CategoryModel extends \Illuminate\Database\Eloquent\Model
{
    protected $table = "content_category";
}