<?php
/**
 * https://gitee.com/Mao02
 * http://www.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2019/4/17
 * Time: 9:57
 */

class ArticleModel extends \Illuminate\Database\Eloquent\Model
{
    protected $table = "content_article";

    protected $primaryKey = 'id';

    public $timestamps = false;

    public function tags(){
        return $this->belongsToMany(TagsModel::class,'content_tags_map','article_id','tag_id')
            ->using(TagsMapModel::class);
    }

    public function replies()
    {
        return $this->hasMany(ReplyModel::class,'article_id','id');
    }

    public function category()
    {
        return $this->belongsTo(CategoryModel::class,'cid','id');
    }
}