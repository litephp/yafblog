<?php
/**
 * https://gitee.com/Mao02
 * http://www.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2019/4/22
 * Time: 10:20
 */

class ConfigModel extends \Illuminate\Database\Eloquent\Model
{
    protected $table = "site_config";
}