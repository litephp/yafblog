<?php
/**
 * https://gitee.com/Mao02
 * http://www.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2019/4/17
 * Time: 14:29
 */

class TagsMapModel extends \Illuminate\Database\Eloquent\Relations\Pivot
{
    protected $table = 'content_tags_map';
}