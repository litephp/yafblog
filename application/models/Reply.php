<?php
/**
 * Created by PhpStorm.
 * User: pure coder
 * Date: 2019/4/21
 * Time: 12:56
 */

class ReplyModel extends \Illuminate\Database\Eloquent\Model
{
    protected $table = "content_article_reply";

    public $timestamps = false;

    public function article()
    {
        return $this->belongsTo(ArticleModel::class,'article_id','id');
    }
}