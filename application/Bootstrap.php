<?php
/**
 * @name Bootstrap
 * @author fanjie
 * @desc 所有在Bootstrap类中, 以_init开头的方法, 都会被Yaf调用,
 * @see http://www.php.net/manual/en/class.yaf-bootstrap-abstract.php
 * 这些方法, 都接受一个参数:Yaf_Dispatcher $dispatcher
 * 调用的次序, 和申明的次序相同
 */
class Bootstrap extends Yaf_Bootstrap_Abstract {

    public function _initErrorReporting()
    {
        error_reporting(0);
    }
    /**
     * 初始化配置
     */
    public function _initConfig() {
		//把配置保存起来
		$arrConfig = Yaf_Application::app()->getConfig();
		Yaf_Registry::set('config', $arrConfig);
    }

    /**
     * 初始化自动加载
     * @param Yaf_Dispatcher $dispatcher
     */
    public function _initLoader(Yaf_Dispatcher $dispatcher)
    {
        $loader = Yaf_Loader::getInstance();
        $loader->import(APPLICATION_PATH.'/vendor/autoload.php');
    }

    /**
     * 初始化调试器
     */
    public function _initDebuger()
    {
        \Tracy\Debugger::enable(null,APPLICATION_PATH.'/log');
    }

    /**
     * 初始化视图引擎
     * @param Yaf_Dispatcher $dispatcher
     */
    public function _initView(Yaf_Dispatcher $dispatcher)
    {
        $dispatcher->autoRender(false);
        $view = new Twig_Adapter(APPLICATION_PATH.'/application/views',[
            'cache'=>APPLICATION_PATH.'/cache/twig',
            'debug' => true
        ]);
        $dispatcher->setView($view);
    }

    /**
     * 初始化数据库抽象层
     */
    public function _initOrm()
    {
        $db_config = Yaf_Registry::get('config')->application->database->toArray();

        $capsule = new Illuminate\Database\Capsule\Manager;
        // 创建链接
        $capsule->addConnection($db_config);

        // 设置全局静态可访问DB
        $capsule->setAsGlobal();

        $capsule->getConnection()->enableQueryLog();

        // 启动Eloquent （如果只使用查询构造器，这个可以注释）
        $capsule->bootEloquent();
    }

    /**
     * 初始化分页器
     */
    public function _initPager(){

        \Illuminate\Pagination\Paginator::currentPathResolver(function () {
            return Yaf_Dispatcher::getInstance()->getRequest()->getRequestUri();
        });

        \Illuminate\Pagination\Paginator::currentPageResolver(function ($pageName = 'page') {
            $page = Yaf_Dispatcher::getInstance()->getRequest()->getQuery($pageName);

            if (filter_var($page, FILTER_VALIDATE_INT) !== false && (int) $page >= 1) {
                return (int) $page;
            }

            return 1;
        });
    }

    /**
     * 初始化插件
     * @param Yaf_Dispatcher $dispatcher
     */
	public function _initPlugin(Yaf_Dispatcher $dispatcher) {
		//注册一个插件
		$objForceRoutePlugin = new ForceRoutePlugin();
		$dispatcher->registerPlugin($objForceRoutePlugin);
	}

    /**
     * 初始化路由
     * @param Yaf_Dispatcher $dispatcher
     */
	public function _initRoute(Yaf_Dispatcher $dispatcher) {
		//在这里注册自己的路由协议,默认使用简单路由
        $route_config = new Yaf_Config_Ini(APPLICATION_PATH . "/conf/route.ini");
        $route = Yaf_Dispatcher::getInstance()->getRouter();
        $route->addConfig($route_config->routes);
	}

    public function _initCache(Yaf_Dispatcher $dispatcher)
    {
        Cache_Service::init();
	}
}
