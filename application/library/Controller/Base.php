<?php
/**
 * https://gitee.com/Mao02
 * http://www.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2019/4/20
 * Time: 11:05
 */

class Controller_Base extends Yaf_Controller_Abstract
{
    protected function assignSeo()
    {
        $site_config = Cache_Service::remember('site_config',function (){
            $config = ConfigModel::get();
            $array = [];
            foreach ($config as $item){
                $array[$item['name']] = $item['value'];
            }
            return $array;
        },86400);
        $this->getView()->assign('site_config',$site_config);
    }

    protected function pageParser($paginator){
        $pager = '';
        if ($paginator->hasPages()){
            $window = \Illuminate\Pagination\UrlWindow::make($paginator);
            $elements = array_filter([
                $window['first'],
                is_array($window['slider']) ? '...' : null,
                $window['slider'],
                is_array($window['last']) ? '...' : null,
                $window['last'],
            ]);
            $pager = $this->getView()->render('_public/pager.phtml',[
                'elements'=>$elements
            ]);
        }
        $this->getView()->assign('pager',$pager);
        return $pager;
    }

    protected function assignCategory(){
        $category = Cache_Service::remember('category',function (){
            return $category = CategoryModel::where('state',1)
                ->where('is_deleted',0)
                ->get();
        },3600);
        $this->getView()->assign('category',$category);
    }

    protected function assignTags()
    {
        $tags = Cache_Service::remember('tags',function (){
            return $tags = TagsModel::get();
        },3600);
        $this->getView()->assign('tags',$tags);
    }

    protected function assignReplies()
    {
        $replies = Cache_Service::remember('replies',function (){
            return $replies = ReplyModel::where('state',1)->limit(10)->get();
        },3600);
        $this->getView()->assign('replies',$replies);
    }
}