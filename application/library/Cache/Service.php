<?php
/**
 * https://gitee.com/Mao02
 * http://www.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2019/4/18
 * Time: 16:10
 */

class Cache_Service
{
    public static function init()
    {
        $options = new \Phpfastcache\Config\ConfigurationOption([
            'path'=>APPLICATION_PATH.'/cache/files',
        ]);
        $cache = new \Phpfastcache\Helper\Psr16Adapter('Files',$options);
        Yaf_Registry::set('cache',$cache);
    }

    public static function remember($name, $value, $expire = null)
    {
        $cache = Yaf_Registry::get('cache');
        if (!$cache->has($name)) {
            if ($value instanceof \Closure) {
                $value = call_user_func($value);
            }
            $cache->set($name, $value, $expire);
        } else {
            $value = $cache->get($name);
        }
        return $value;
    }
    
    public static function __callStatic($name,$args)
    {
        $cache = Yaf_Registry::get('cache');
        return call_user_func_array([$cache,$name],$args);
    }
}