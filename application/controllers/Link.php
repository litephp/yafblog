<?php
/**
 * https://gitee.com/Mao02
 * http://www.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2019/4/16
 * Time: 17:56
 */

class LinkController extends Controller_Base
{
    public function indexAction()
    {
        $this->assignCategory();

        $this->assignSeo();

        $links = Cache_Service::remember('links',function (){
            return $links = LinkModel::where('state',1)
                ->get();
        },3600);
        $this->getView()->assign('links',$links);
        $this->getView()->display('link/index.phtml');
    }
}