<?php
/**
 * https://gitee.com/Mao02
 * http://www.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2019/4/16
 * Time: 17:56
 */

class TagsController extends Controller_Base
{
    public function indexAction()
    {
        $id = $this->getRequest()->getParam('id');

        $tag = TagsModel::where('id',$id)->first();

        $this->getView()->assign('tag',$tag);

        $ids = TagsMapModel::where('tag_id',$id)->pluck('article_id');

        $articles = ArticleModel::whereIn('id', $ids->toArray())
            ->where('state',1)
            ->where('is_deleted',0)
            ->orderBy('id','desc')
            ->paginate(15);

        $this->getView()->assign('articles',$articles);

        $this->pageParser($articles);

        $this->assignCategory();

        $this->assignTags();

        $this->assignReplies();

        $this->assignSeo();

        $this->getView()->display('tags/index.phtml');
    }
}