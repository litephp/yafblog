<?php
/**
 * https://gitee.com/Mao02
 * http://www.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2019/4/16
 * Time: 17:56
 */

class CategoryController extends Controller_Base
{
    public function indexAction()
    {
        $id = $this->getRequest()->getParam('id');

        $category_title = CategoryModel::where('id',$id)->first();

        $this->getView()->assign('category_title',$category_title);

        $articles = ArticleModel::where('cid',$id)
            ->where('state',1)
            ->where('is_deleted',0)
            ->orderBy('id','desc')
            ->paginate(15);

        $this->pageParser($articles);

        $this->getView()->assign('articles',$articles);

        $this->assignCategory();

        $this->assignTags();

        $this->assignReplies();

        $this->assignSeo();

        $this->getView()->display('category/index.phtml');
    }
}