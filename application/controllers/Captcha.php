<?php
/**
 * Created by PhpStorm.
 * User: pure coder
 * Date: 2019/4/23
 * Time: 21:52
 */

class CaptchaController extends Yaf_Controller_Abstract
{
    public function createAction()
    {
        $phrase = new \Gregwar\Captcha\PhraseBuilder(4,'1234567890');
        $builder = new Gregwar\Captcha\CaptchaBuilder(null, $phrase);
        $builder->build();
        session_start();
        $_SESSION['verify_code'] = $builder->getPhrase();
//        var_dump($_SESSION);
        header('Content-type: image/jpeg');
        $builder->output();
    }
}