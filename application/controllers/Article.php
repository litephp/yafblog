<?php
/**
 * https://gitee.com/Mao02
 * http://www.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2019/4/16
 * Time: 17:56
 */

class ArticleController extends Controller_Base
{
    public function indexAction()
    {
        $id = $this->getRequest()->getParam('id');

        $article = ArticleModel::with('replies')->where('id',$id)->where('state',1)->where('is_deleted',0)->first();
        if (empty($article)){
            http_response_code('404');
            return $this->getResponse()->setBody('<h1>对不起，没有找到你要的数据</h1>');
        }

        $article->click += 1;

        $article->save();

        $this->getView()->assign('article',$article);

        $this->assignReplies();

        $this->assignTags();

        $this->assignCategory();

        $this->assignSeo();

        $this->getView()->display('article/index.phtml');
    }
}