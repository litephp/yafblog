<?php
/**
 * Created by PhpStorm.
 * User: pure coder
 * Date: 2019/4/21
 * Time: 15:59
 */

class ReplyController extends Controller_Base
{
    public function commitAction()
    {
        $post = $this->getRequest()->getPost();
        session_start();

        if ($_SESSION['verify_code'] !== $post['verify_code']){
            $error =<<<EOT
<h1>验证码错误！</h1>
<p><span id="time">3</span> 秒跳转</p>
<script>
timenum = 3;
setInterval(function() {
    if (timenum > 0){
        timenum--;
        time.innerText = timenum;
    }else{
        history.go(-1);
    } 
},1000);
</script>
EOT;
            die($error);
        }
        $user_IP = ($_SERVER["HTTP_VIA"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];
        $user_IP = ($user_IP) ? $user_IP : $_SERVER["REMOTE_ADDR"];
        $user_IP;
        $referer = $_SERVER['HTTP_REFERER'];
        $reply = new ReplyModel();
        $reply->article_id = $post['article_id'];
        $reply->qq = $post['qq'];
        $reply->website = $post['website'];
        $reply->name = $post['name'];
        $reply->content = htmlspecialchars($post['content']);
        $reply->create_time = time();
        $reply->state = 0;
        $reply->ip = ip2long($user_IP);
        $res = $reply->save();
        unset($_SESSION['verify_code']);
        $success =<<<EOT
<h1>留言成功！博主审核后会进行展示！</h1>
<p><span id="time">3</span> 秒跳转</p>
<script>
timenum = 3;
setInterval(function() {
    if (timenum > 0){
        timenum--;
        time.innerText = timenum;
    }else{
        location.href="{$referer}";
    } 
},1000);
</script>
EOT;
        die($success);
    }
}