<?php
/**
 * https://gitee.com/Mao02
 * http://www.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2019/4/17
 * Time: 9:51
 */

class AboutController extends Controller_Base
{
    public function indexAction()
    {
        $this->assignCategory();

        $this->assignSeo();

        $this->getView()->display('about/index.phtml');
    }
}