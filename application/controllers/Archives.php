<?php
/**
 * https://gitee.com/Mao02
 * http://www.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2019/4/16
 * Time: 17:56
 */

class ArchivesController extends Controller_Base
{
    public function indexAction()
    {
        $this->assignCategory();

        $this->assignTags();

        $this->assignReplies();

        $this->assignSeo();

        $articles = ArticleModel::where('state',1)
            ->where('is_deleted',0)
            ->orderBy('id','desc')
            ->get();
        $this->getView()->assign('articles',$articles);
        $this->getView()->display('archives/index.phtml');
    }
}