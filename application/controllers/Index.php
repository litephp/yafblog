<?php
/**
 * @name Index
 * @author fanjie
 * @desc 默认控制器
 * @see http://www.php.net/manual/en/class.yaf-controller-abstract.php
 */
class IndexController extends Controller_Base {

    /**
     * 默认动作
     * Yaf支持直接把Yaf_Request_Abstract::getParam()得到的同名参数作为Action的形参
     * 对于如下的例子, 当访问http://yourhost/blog/index/index/index/name/litephp 的时候, 你就会发现不同
     */
    public function indexAction()
    {
        $this->assignSeo();

        $this->assignCategory();

        $this->assignTags();

        $this->assignReplies();

        $articles = ArticleModel::where('state',1)
            ->where('is_deleted',0)
            ->orderBy('id','desc')
            ->paginate(15);

        $this->getView()->assign('articles',$articles);

        $this->pageParser($articles);

        $this->getView()->display('index/index.phtml');

    }
}
