$(function () {
    $(document).on('click','a[target!="_blank"]',function (ev) {
        if(history.pushState){
            ev.preventDefault();
            var url = $(ev.target).attr('href');
            if (url === location.pathname){
                return;
            }

            $.ajax({
                url:url,
                method:'get'
            }).done(function (data) {
                var html = $(data).find('#pjax-container').html();
                var title = $(data).filter('title').text();
                $('#pjax-container').html(html);
                document.title = title;
                window.history.pushState(200, title, url);
            })
        }
    });

    $(document).on('submit','form[search="article"]',function (ev) {
        if(history.pushState){
            ev.preventDefault();
            console.log(ev);
            var url = $(ev.target).attr('action');
            var data = $(ev.target).serialize();
            $.ajax({
                url:url+'?'+data,
                method:'get'
            }).done(function (data) {
                if(data.code === 0){
                    alert('不好意思出现故障了！');
                    console.log(data.msg);
                    return;
                }

                var html = $(data).find('#pjax-container').html();
                var title = $(data).filter('title').text();
                $('#pjax-container').html(html);
                document.title = title;
                window.history.pushState(200, title, url);
            })
        }
    });
})