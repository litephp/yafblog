function zplayer(e) {
    this.isMobile = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i),
    this.isMobile && (e.autoPlay = !1);
    var a = {
        autoPlay: !1,
        lrcStart: !1,
        showLrc: !1,
        fixed: !1,
        listFolded: !1,
        listMaxHeight: 240
    };
    for (var t in a) {
        a.hasOwnProperty(t) && !e.hasOwnProperty(t) && (e[t] = a[t])
    }
    this.option = e
}
zplayer.prototype.init = function() {
    var p = this, y, cur_m = 0, random_cur_m=0, m="", musicListHtml="",ht="",playType=0,curPlayType=0,playTypes=["z-icon-retweet","z-icon-retweet-one","z-icon-random","z-icon-reorder-list"],playList=[],playListRandom=[];
    this.element = this.option.element;
    this.musics = this.option.musics;
    for (var i = 0; i < this.musics.length; i++) {
        playList.push(i);
        i == 0 ? musicListHtml += '<li class="playing" index="' + i + '">' + this.musics[i].title + "&nbsp; - &nbsp;" + this.musics[i].author + "</li>" : musicListHtml += '<li index="' + i + '">' + this.musics[i].title + "&nbsp; - &nbsp;" + this.musics[i].author + "</li>"
    }
    ht+= '<div class="zplayer">'
        +'	<div class="zplayer-content">'
        +'		<div class="zplayer-pic">'
        +'			<img src="' + this.musics[0].pic + '">'
        +'		</div>'
        +'		<div class="zplayer-info">'
        +'			<div class="zplayer-music">'
        +'				<span class="zplayer-title">' + this.musics[0].title + '</span>'
        +'				<span class="zplayer-author"> - ' + this.musics[0].author + '</span>'
        +'				<i class="zplayer-list-btn z-icon z-icon-reorder">'
        +'				</i>'
        +'			</div>';
    if(!this.option.fixed){
        ht+= this.option.showLrc ? '<div class="zlrc">' : '<div class="zlrc" style="display:none">';
        ht+= '	<div class="zplayer-lrc">'
            +'		<div class="zplayer-lrc-contents" style="transform: translateY(0);">'
            +'		</div>'
            +'	</div>'
            +'</div>';
    }
    ht+= '			<div class="zplayer-controller">'
        +'				<div class="zplayer-bar-wrap">'
        +'					<div class="zplayer-bar">'
        +'						<div class="zplayer-loaded" style="width: 0"></div>'
        +'						<div class="zplayer-played" style="width: 0">'
        +'							<span class="zplayer-thumb"></span>'
        +'						</div>'
        +'					</div>'
        +'				</div>'
        +'				<div class="zplayer-time">'
        +'					<span class="zplayer-ptime">00:00</span>/<span class="zplayer-dtime">00:00</span>'
        +'					<div class="zplayer-menu-bars">'
        +'						<i class="z-icon z-icon-backward"></i>'
        +'						<i class="z-icon z-icon-play"></i>'
        +'						<i class="z-icon z-icon-pause display-none"></i>'
        +'						<i class="z-icon z-icon-forward"></i>'
        +'					</div>'
        +'					<div class="zplayer-menu-volume">';
    var volumeCoocie=getCookie("zplayer-volume");
    var volInit= volumeCoocie=="" ? "80%" : volumeCoocie*100+"%";
    var volIcon= volumeCoocie==""?"z-icon-volume-up":(volumeCoocie >=0.8 ? "z-icon-volume-up" :(volumeCoocie==0?"z-icon-volume-off":"z-icon-volume-down"))
    ht += this.isMobile ? '' : '<i class="volume-icon z-icon '+volIcon+'"></i><div class="zplayer-volume-bar"><div class="zplayer-volume-played" style="width:'+volInit+'"><span class="zplayer-volume-thumb"></span></div></div>';
    ht += this.option.lrcStart ? (this.option.showLrc ? '<i class="lrc-icon z-icon z-icon-list-alt"></i>' : '<i class="lrc-icon z-icon z-icon-list-alt lrc-hide"></i>') : '';
    ht +='<i class="player-type-icon z-icon z-icon-retweet"></i>'
        +'					</div>'
        +'				</div>'
        +'			</div>'
        +'		</div>'
        +'	</div>';
    ht += this.option.fixed ? '<div class="ss-btn"><i class="z-icon z-icon-chevron-left display-none"></i><i class="z-icon z-icon-chevron-right"></i></div>' : '';
    ht += this.option.listFolded ? '<ol style="max-height:0px;" class="zplayer-list">' + musicListHtml + '</ol>' : '<ol style="max-height:'+this.option.listMaxHeight+'px;" class="zplayer-list">' + musicListHtml + '</ol>';
    ht += '</div>';
    if(this.option.fixed){
        ht+= this.option.showLrc ? '<div class="zlrc">' : '<div class="zlrc" style="display:none">';
        ht+= '	<div class="zplayer-lrc">'
            +'		<div class="zplayer-lrc-contents" style="transform: translateY(0);">'
            +'		</div>'
            +'	</div>'
            +'</div>';
    }
    this.element.innerHTML=ht;
    if(this.option.fixed){
        this.element.classList.add("zplayer-fixed");
    }
    if (this.option.lrcStart) {
        this.lrc = l(this.musics[0].lrc);
        if(this.option.showLrc){
            this.element.classList.add("zplayer-withlrc");
        }
        this.lrcContents = this.element.getElementsByClassName("zplayer-lrc-contents")[0];
        for (d = 0; d < this.lrc.length; d++){
            m += "<p>" + this.lrc[d][1] + "</p>";
        }
        this.lrcContents.innerHTML = m;
        this.lrcIndex = 0;
        this.lrcContents.getElementsByTagName("p")[0].classList.add("zplayer-lrc-current");
    }
    this.audio = document.createElement("audio");
    this.audio.src = this.musics[0].url;
    this.audio.loop = 0;
    this.audio.preload = "metadata";
    this.audio.volume = volumeCoocie=="" ? 0.8 : volumeCoocie ;
    this.audio.addEventListener("durationchange", function() {
        1 !== p.audio.duration && (p.element.getElementsByClassName("zplayer-dtime")[0].innerHTML = p.secondToTime(p.audio.duration))
    }),
        this.audio.addEventListener("loadedmetadata", function() {
            p.loadedTime = setInterval(function() {
                var e = p.audio.buffered.end(p.audio.buffered.length - 1) / p.audio.duration;
                p.updateBar.call(p, "loaded", e, "width");
                1 === e && clearInterval(p.loadedTime);
            }, 500)
        })
    this.audio.addEventListener("ended", function() {
        if(playType==0){
            cur_m = ((cur_m + 1) == p.musics.length) ? 0 : cur_m + 1;
            playSwitch(cur_m);
        }else if(playType==1){
            playSwitch(cur_m);
        }else if(playType==2){
            if(curPlayType!=playType){
                for(var i=0;i<playListRandom.length;i++){
                    if(cur_m==playListRandom[i]){
                        random_cur_m=i+1;
                        cur_m=playListRandom[random_cur_m];
                        break;
                    }
                }
            }else{
                random_cur_m = ((random_cur_m + 1) == p.musics.length) ? 0 : random_cur_m + 1;
                cur_m=playListRandom[random_cur_m];
            }
            playSwitch(cur_m);
        }else if(playType==3){
            if(cur_m != (p.musics.length-1)){
                cur_m = ((cur_m + 1) == p.musics.length) ? 0 : cur_m + 1;
                playSwitch(cur_m);
            }else{
                p.PauseButton.classList.add("display-none");
                p.PlayButton.classList.remove("display-none");
                p.audio.pause();
                clearInterval(this.playedTime)
            }
        }
        curPlayType=playType;
    })
    this.audio.addEventListener("error", function() {
        p.element.getElementsByClassName("zplayer-author")[0].innerHTML = " - 加载失败 ╥﹏╥";
        p.element.getElementsByClassName("zplayer-dtime")[0].innerHTML = "00:00";
        p.PauseButton.classList.add("display-none");
        p.PlayButton.classList.remove("display-none")
    })
    this.PlayButton = this.element.getElementsByClassName("z-icon-play")[0];
    this.PauseButton = this.element.getElementsByClassName("z-icon-pause")[0];
    this.volumeIcon = this.element.getElementsByClassName("volume-icon")[0];
    this.lrcIcon = this.element.getElementsByClassName("lrc-icon")[0];
    this.playTypeIcon = this.element.getElementsByClassName("player-type-icon")[0];
    this.hidePlayerButton = this.element.getElementsByClassName("z-icon-chevron-left")[0];
    this.showPlayerButton = this.element.getElementsByClassName("z-icon-chevron-right")[0];
    this.musicList = this.element.getElementsByClassName("zplayer-list")[0].getElementsByTagName("li");
    this.playedBar = this.element.getElementsByClassName("zplayer-played")[0];
    this.loadedBar = this.element.getElementsByClassName("zplayer-loaded")[0];
    this.thumb = this.element.getElementsByClassName("zplayer-thumb")[0];
    this.bar = this.element.getElementsByClassName("zplayer-bar")[0];
    this.volumeThumb = this.element.getElementsByClassName("zplayer-volume-thumb")[0];
    this.volumeBar = this.element.getElementsByClassName("zplayer-volume-bar")[0];
    this.volumePlayedBar = this.element.getElementsByClassName("zplayer-volume-played")[0];
    this.listButton = this.element.getElementsByClassName("zplayer-list-btn")[0];
    this.playerList = this.element.getElementsByClassName("zplayer-list")[0];
    this.bw = p.element.getElementsByClassName("zplayer-menu-bars")[0].getElementsByClassName("z-icon-backward")[0];
    this.fw = p.element.getElementsByClassName("zplayer-menu-bars")[0].getElementsByClassName("z-icon-forward")[0];
    for (var i = 0; i < this.musicList.length; i++) {
        this.musicList[i].addEventListener("click", function() {
            cur_m=parseInt(this.getAttribute("index"));
            if(playType==2){
                for(var i=0;i<playListRandom.length;i++){
                    if(cur_m==playListRandom[i]){
                        random_cur_m=i;
                    }
                }
            }
            playSwitch(cur_m);
            curPlayType=playType;
        })
    }
    this.PlayButton.addEventListener("click", function() {
        p.play.call(p)
    })
    this.PauseButton.addEventListener("click", function() {
        p.pause.call(p)
    })
    if(!this.isMobile){
        this.volumeIcon.addEventListener("click", function() {
            p.audio.muted ? (p.audio.muted = !1,
                p.volumeIcon.className = 0.8 <= p.audio.volume ? "volume-icon z-icon z-icon-volume-up" : "volume-icon z-icon z-icon-volume-down",
                p.updateBar.call(p, "volumePlayed", p.audio.volume, "width")) : (p.audio.muted = !0,
                p.volumeIcon.className = "volume-icon z-icon z-icon-volume-off",
                p.updateBar.call(p, "volumePlayed", 0, "width"))
        })
    }
    if(this.option.lrcStart){
        this.lrcIcon.addEventListener("click", function() {
            this.classList.contains("lrc-hide") ? (p.element.classList.add("zplayer-withlrc"),this.classList.remove("lrc-hide"),p.element.getElementsByClassName("zlrc")[0].style.display="block") : (p.element.classList.remove("zplayer-withlrc"),this.classList.add("lrc-hide"),p.element.getElementsByClassName("zlrc")[0].style.display="none");
        })
    }
    if(this.option.fixed){
        this.hidePlayerButton.addEventListener("click", function() {
            p.element.getElementsByClassName("zplayer")[0].style.transform = "translateX(-355px)";
            p.hidePlayerButton.classList.add("display-none");
            p.showPlayerButton.classList.remove("display-none")
        })
        this.showPlayerButton.addEventListener("click", function() {
            p.element.getElementsByClassName("zplayer")[0].style.transform = "translateX(0)";
            p.showPlayerButton.classList.add("display-none");
            p.hidePlayerButton.classList.remove("display-none")
        })
    }
    this.playTypeIcon.addEventListener("click", function() {
        p.playTypeIcon.classList.remove(playTypes[playType]);
        playType=(playType+1)%4;
        p.playTypeIcon.classList.add(playTypes[playType]);
        if(playType==2){
            playListRandom=shuffle(playList);
        }
    })
    this.bar.addEventListener("click", function(e) {
        var a = e || window.event;
        y = p.bar.clientWidth;
        var i = (a.clientX - t(p.bar)) / y;
        p.updateBar.call(p, "played", i, "width");
        p.element.getElementsByClassName("zplayer-ptime")[0].innerHTML = p.secondToTime(i * p.audio.duration);
        p.audio.currentTime = parseFloat(p.playedBar.style.width) / 100 * p.audio.duration
    })
    this.thumb.addEventListener("mousedown", function() {
        y = p.bar.clientWidth;
        clearInterval(p.playedTime);
        document.addEventListener("mousemove", e);
        document.addEventListener("mouseup", a)
    })
    if(!this.isMobile){
        this.volumeThumb.addEventListener("mousedown", function() {
            y = p.volumeBar.clientWidth;
            document.addEventListener("mousemove", voe);
            document.addEventListener("mouseup", voa)
        })
    }
    if(!this.isMobile){
        this.volumeBar.addEventListener("click", function(e) {
            var a = e || window.event;
            y = p.volumeBar.clientWidth;
            i = (a.clientX - t(p.volumeBar)) / y;
            p.volumeIcon.className = 0.8 <= i ? "volume-icon z-icon z-icon-volume-up" : (i==0 ? "volume-icon z-icon z-icon-volume-off" : "volume-icon z-icon z-icon-volume-down")
            p.audio.volume = i;
            p.updateBar.call(p, "volumePlayed", i, "width");
        })
    };
    this.listButton.addEventListener("click", function(e) {
        (p.playerList.style.maxHeight == "" || p.playerList.style.maxHeight == "0px") ? p.playerList.style.maxHeight = p.option.listMaxHeight+"px" : p.playerList.style.maxHeight = "0px"
    })
    this.bw.addEventListener("click", function() {
        pn(1);
    })
    this.fw.addEventListener("click", function() {
        pn(2);
    })

    this.option.autoPlay && this.play();

    function e(e) {
        var a = e || window.event, i = (a.clientX - t(p.bar)) / y;
        i = i > 0 ? i : 0;
        i = 1 > i ? i : 1;
        p.updateBar.call(p, "played", i, "width");
        p.option.lrcStart && p.updateLrc.call(p, parseFloat(p.playedBar.style.width) / 100 * p.audio.duration);
        p.element.getElementsByClassName("zplayer-ptime")[0].innerHTML = p.secondToTime(i * p.audio.duration)
    }
    function a() {
        document.removeEventListener("mouseup", a);
        document.removeEventListener("mousemove", e);
        p.audio.currentTime = parseFloat(p.playedBar.style.width) / 100 * p.audio.duration;
        p.play()
    }
    function t(e) {
        for (var a, t = e.offsetLeft, i = e.offsetParent; null !== i; ) {
            t += i.offsetLeft,
                i = i.offsetParent
        }
        return a = document.body.scrollLeft + document.documentElement.scrollLeft,
        t - a
    }
    function i(e) {
        for (var a, t = e.offsetTop, i = e.offsetParent; null !== i; ) {
            t += i.offsetTop,
                i = i.offsetParent
        }
        return a = document.body.scrollTop + document.documentElement.scrollTop,
        t - a
    }
    function voe(e) {
        var a = e || window.event
            , i = (a.clientX - t(p.volumeBar)) / y;
        i = i > 0 ? i : 0;
        i = 1 > i ? i : 1;
        if(p.audio.muted){
            p.audio.muted = !1;
        }
        p.audio.volume = i;
        p.updateBar.call(p, "volumePlayed", i, "width");
        if (i >= 0.8) {
            p.volumeIcon.classList.remove("z-icon-volume-down", "z-icon-volume-off");
            p.volumeIcon.classList.add("z-icon-volume-up")
        } else {
            if (i == 0) {
                p.volumeIcon.classList.remove("z-icon-volume-down", "z-icon-volume-up");
                p.volumeIcon.classList.add("z-icon-volume-off")
            } else {
                p.volumeIcon.classList.remove("z-icon-volume-off", "z-icon-volume-up");
                p.volumeIcon.classList.add("z-icon-volume-down")
            }
        }
        setCookie("zplayer-volume",i,365);
    }
    function voa() {
        document.removeEventListener("mousemove", voe);
    }
    function l(e) {
        if (e) {
            for (var t = (e = e.replace(/([^\]^\n])\[/g, function(e, t) {
                return t + "\n["
            })).split("\n"), n = [], i = t.length, a = 0; a < i; a++) {
                var r = t[a].match(/\[(\d{2}):(\d{2})(\.(\d{2,3}))?]/g)
                    , o = t[a].replace(/.*\[(\d{2}):(\d{2})(\.(\d{2,3}))?]/g, "").replace(/<(\d{2}):(\d{2})(\.(\d{2,3}))?>/g, "").replace(/^\s+|\s+$/g, "");
                if (r)
                    for (var s = r.length, l = 0; l < s; l++) {
                        var u = /\[(\d{2}):(\d{2})(\.(\d{2,3}))?]/.exec(r[l])
                            , c = 60 * u[1] + parseInt(u[2]) + (u[4] ? parseInt(u[4]) / (2 === (u[4] + "").length ? 100 : 1e3) : 0);
                        n.push([c, o])
                    }
            }
            return (n = n.filter(function(e) {
                return e[1]
            })).sort(function(e, t) {
                return e[0] - t[0]
            }),
                n
        }
        return [[0,"暂无歌词，请您欣赏"]]
    }
    function setCookie(cname,cvalue,exdays){
        var d = new Date();
        d.setTime(d.getTime()+(exdays*24*60*60*1000));
        var expires = "expires="+d.toGMTString();
        document.cookie = cname+"="+cvalue+"; "+expires;
    }
    function getCookie(cname){
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i].trim();
            if (c.indexOf(name)==0) { return c.substring(name.length,c.length); }
        }
        return "";
    }
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min)
    }
    function shuffle(arr) {
        var _arr = arr.slice()
        for (var i = 0; i < _arr.length; i++) {
            var j = getRandomInt(0, i)
            var t = _arr[i]
            _arr[i] = _arr[j]
            _arr[j] = t
        }
        return _arr
    }
    function playSwitch(i) {
        p.PlayButton.classList.add("display-none");
        p.PauseButton.classList.remove("display-none");
        p.element.getElementsByClassName("zplayer-pic")[0].getElementsByTagName("img")[0].src = p.musics[i].pic;
        p.element.getElementsByClassName("zplayer-title")[0].innerHTML = p.musics[i].title;
        p.element.getElementsByClassName("zplayer-author")[0].innerHTML = " - " + p.musics[i].author;
        p.element.getElementsByClassName("zplayer-list")[0].getElementsByClassName("playing")[0].classList.remove("playing");
        p.element.getElementsByClassName("zplayer-list")[0].getElementsByTagName("li")[i].classList.add("playing");
        if (p.option.lrcStart) {
            p.lrcContents.innerHTML = "";
            p.lrcContents.style.transform = "translateY(0px)";
            m = "";
            p.lrc = l(p.musics[i].lrc);
            for (d = 0; d < p.lrc.length; d++) {
                m += "<p>" + p.lrc[d][1] + "</p>";
                p.lrcContents.innerHTML = m;
                p.lrcIndex = 0;
                p.lrcContents.getElementsByTagName("p")[0].classList.add("zplayer-lrc-current");
            }
        }
        clearInterval(p.loadedTime);
        clearInterval(p.playedTime);
        p.audio.src = p.musics[i].url;
        p.audio.play();
        p.playedTime = setInterval(function() {
            p.updateBar.call(p, "played", p.audio.currentTime / p.audio.duration, "width"),
            p.option.lrcStart && p.updateLrc.call(p),
                p.element.getElementsByClassName("zplayer-ptime")[0].innerHTML = p.secondToTime(p.audio.currentTime)
        }, 100)
    }

    function pn(e){
        if(playType==2){
            if(curPlayType!=playType){
                for(var i=0;i<playListRandom.length;i++){
                    if(cur_m==playListRandom[i]){
                        random_cur_m=i+1;
                        cur_m=playListRandom[i+1];
                        break;
                    }
                }
                curPlayType=playType;
            }else{
                random_cur_m = (e==1 ? ((random_cur_m == 0) ? p.musics.length - 1 : random_cur_m - 1) : ((random_cur_m == (p.musics.length - 1)) ? 0 : random_cur_m + 1));
                cur_m=playListRandom[random_cur_m];
            }
        }else{
            cur_m = (e==1 ? ((cur_m == 0) ? p.musics.length - 1 : cur_m - 1) : ((cur_m == (p.musics.length - 1)) ? 0 : cur_m + 1));
        }
        curPlayType=playType;
        playSwitch(cur_m)
    }
}
zplayer.prototype.play = function() {
    this.PlayButton.classList.add("display-none");
    this.PauseButton.classList.remove("display-none");
    this.audio.play();
    var e = this;
    this.playedTime = setInterval(function() {
        e.updateBar.call(e, "played", e.audio.currentTime / e.audio.duration, "width");
        e.option.lrcStart && e.updateLrc.call(e),
            e.element.getElementsByClassName("zplayer-ptime")[0].innerHTML = e.secondToTime(e.audio.currentTime)
    }, 100)
}
zplayer.prototype.pause = function() {
    this.PauseButton.classList.add("display-none");
    this.PlayButton.classList.remove("display-none");
    this.audio.pause();
    clearInterval(this.playedTime)
}
zplayer.prototype.updateBar = function(e, a, t) {
    a = a > 0 ? a : 0,
        a = 1 > a ? a : 1,
        this[e + "Bar"].style[t] = 100 * a + "%"
}
zplayer.prototype.updateLrc = function(e) {
    if (e || (e = this.audio.currentTime),
    e < this.lrc[this.lrcIndex][0] || ( this.lrcIndex == this.lrc.length-1 ? false : e >= this.lrc[this.lrcIndex + 1][0] ) )
        for (var a = 0; a < this.lrc.length; a++)
            e >= this.lrc[a][0] && (!this.lrc[a + 1] || e < this.lrc[a + 1][0]) && (this.lrcIndex = a,
                this.lrcContents.style.transform = "translateY(" + 20 * -this.lrcIndex + "px)",
                this.lrcContents.getElementsByClassName("zplayer-lrc-current")[0].classList.remove("zplayer-lrc-current"),
                this.lrcContents.getElementsByTagName("p")[a].classList.add("zplayer-lrc-current"))
}
zplayer.prototype.secondToTime = function(e) {
    var a = function(e) {
        return 10 > e ? "0" + e : "" + e
    }
        , t = parseInt(e / 60)
        , i = parseInt(e - 60 * t);
    return a(t) + ":" + a(i)
}
