# yafblog

#### 介绍
我的自用博客


#### 软件架构
基于yaf编写，后台与[liteadmin2.0](https://gitee.com/Mao02/liteadmin)对接


#### 安装教程

```$xslt
git clone https://gitee.com/Mao02/yafblog.git

cd yafblog

composer update
```

#### 使用说明

数据库结构请参照[liteadmin2.0](https://gitee.com/Mao02/liteadmin)

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)